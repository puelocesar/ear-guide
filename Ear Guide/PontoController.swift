//
//  PontoController.swift
//  Ear Guide
//
//  Created by Paulo Cesar on 21/01/17.
//  Copyright © 2017 Paulo Cesar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PontoController: UIViewController {

    @IBOutlet var titulo: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var descricao: UITextView!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var startTimeLabel: UILabel!
    @IBOutlet var progressSlider: UISlider!
    @IBOutlet var endTimeLabel: UILabel!
    
    let player = AVPlayer()
    var playbackTimeObserver: Any?
    
    var totalDuration = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        player.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions(), context: nil)
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { notification in
            self.playButton.isSelected = false
        }
        
        playbackTimeObserver = player.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, Int32(NSEC_PER_SEC)), queue: nil) { (time: CMTime) in
            
            let decorrido = Int(CMTimeGetSeconds(time))
            
            self.startTimeLabel.text = self.timeFormatFor(segundos: decorrido)
            self.progressSlider.value = Float(decorrido)
            
            let falta = self.totalDuration-decorrido
            
            self.endTimeLabel.text = "-\(self.timeFormatFor(segundos: falta))"
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
    }
    
    func setupWith(ponto: Ponto) {
        titulo.text = ponto.nome
        descricao.text = ponto.descricao
        
        for view in scrollView.subviews {
            view.removeFromSuperview()
        }
        
        let ssize = scrollView.frame.size
        var x = CGFloat(16)
        for fotoName in ponto.fotos {
            if let path = Bundle.main.path(forResource: fotoName, ofType: "jpg") {
                if let image = UIImage(contentsOfFile: path) {
                    let imageWith = (ssize.height/image.size.height)*image.size.width
                    
                    let imgv = UIImageView(image: image)
                    imgv.frame = CGRect(x: x, y: 0, width: imageWith, height: ssize.height)
                    
                    x += imageWith + 16
                    
                    scrollView.addSubview(imgv)
                }
            }
        }
        
        scrollView.contentSize = CGSize(width: x, height: ssize.height)
        
        if let path = Bundle.main.path(forResource: ponto.audio, ofType: "mp3") {
            let audioAsset = AVAsset(url: URL(fileURLWithPath: path))
            totalDuration = Int(CMTimeGetSeconds(audioAsset.duration))
            
            endTimeLabel.text = "-\(timeFormatFor(segundos: totalDuration))"
            progressSlider.maximumValue = Float(totalDuration)
            
            player.replaceCurrentItem(with: AVPlayerItem(asset: audioAsset))
            player.play()
            
            playButton.isSelected = true
        }
    }
    
    func calculateHeight() -> CGFloat {
        let constraintRect = CGSize(width: descricao.frame.size.width, height: .greatestFiniteMagnitude)
        var range = NSRange(location: 0, length: descricao.attributedText.length)
        let boundingBox = descricao.text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: descricao.attributedText.attributes(at: 0, effectiveRange: &range), context: nil)
        
        return 450-(150-boundingBox.height);
    }
    
    func timeFormatFor(segundos: Int) -> String {
        return "\(String(format: "%02d", segundos/60)):\(String(format: "%02d", segundos%60))"
    }
    
    @IBAction func playPause(_ sender: Any) {
        if playButton.isSelected {
            player.pause()
        }
        else {
            player.play()
        }
        
        playButton.isSelected = !playButton.isSelected
    }
    
    @IBAction func track(_ sender: UISlider) {
        player.seek(to: CMTimeMake(Int64(sender.value), 1))
    }
}
