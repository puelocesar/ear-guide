//
//  DataImporter.swift
//  Ear Guide
//
//  Created by Paulo Cesar on 19/01/17.
//  Copyright © 2017 Paulo Cesar. All rights reserved.
//

import UIKit
import MapKit

import Argo
import Curry
import Runes

struct Ponto {
    let nome: String
    let fotos: [String]
    let audio: String
    let descricao: String
    let latitude: Double
    let longitude: Double
    var polyline: [CLLocationCoordinate2D]?
}

extension CLLocationCoordinate2D: Decodable {
    public static func decode(_ j: JSON) -> Decoded<CLLocationCoordinate2D> {
        return curry(CLLocationCoordinate2D.init)
            <^> j <| "latitude"
            <*> j <| "longitude"
    }
    
    func location() -> CLLocation {
        return CLLocation(latitude: self.latitude, longitude: self.longitude)
    }
}

extension Ponto: Decodable {
    public static func decode(_ j: JSON) -> Decoded<Ponto> {
        return curry(Ponto.init)
            <^> j <| "nome"
            <*> j <|| "fotos"
            <*> j <| "audio"
            <*> j <| "descricao"
            <*> j <| "latitude"
            <*> j <| "longitude"
            <*> j <||? "polyline"
    }
}

struct Rota {
    let nome: String
    let tamanho: Int
    let tempo: Int
    let descricao: String
    let pontos: [Ponto]
}

extension Rota: Decodable {
    public static func decode(_ j: JSON) -> Decoded<Rota> {
        return curry(Rota.init)
            <^> j <| "nome"
            <*> j <| "tamanho"
            <*> j <| "tempo"
            <*> j <| "descricao"
            <*> j <|| "pontos"
    }
}

class PontoAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var order: Int
    var info: Ponto
    var visited = false
    
    init(ponto: Ponto, ordem: Int) {
        coordinate = CLLocationCoordinate2DMake(ponto.latitude, ponto.longitude)
        title = ponto.nome
        subtitle = ponto.descricao
        order = ordem
        info = ponto
    }
}

class DataImporter: NSObject {
    
    let pontos: [PontoAnnotation]?
    
    override init() {
        if let data = try? Data(contentsOf: Bundle.main.url(forResource: "guia", withExtension: "json")!) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let rota: Rota = decode(json) {
                    var i = -1
                    self.pontos = rota.pontos.map({ (ponto) -> PontoAnnotation in
                        i+=1
                        return PontoAnnotation(ponto: ponto, ordem: i)
                    })
                    return
                }
            }
            catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        
        self.pontos = nil
    }
}

extension UIColor {
    public static func with(r: Int, g: Int, b: Int) -> UIColor {
        return UIColor(red: CGFloat(Double(r)/255.0), green: CGFloat(Double(g)/255.0), blue: CGFloat(Double(b)/255.0), alpha: 1)
    }
}
