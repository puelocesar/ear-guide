//
//  MapUtilController.swift
//  Ear Guide
//
//  Created by Paulo Cesar on 21/01/17.
//  Copyright © 2017 Paulo Cesar. All rights reserved.
//

import UIKit
import MapKit

class MapUtilController: UIViewController, UITextFieldDelegate, MKMapViewDelegate {

    @IBOutlet var map: MKMapView!
    @IBOutlet var addressField: UITextField!
    
    var pontos: [PontoAnnotation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let layer = MKTileOverlay(urlTemplate: "https://a.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png")
        layer.canReplaceMapContent = true
        map.add(layer)
        
        // Do any additional setup after loading the view.
        addressField.delegate = self
        map.delegate = self
        
        map.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(MapUtilController.handleLongpress(recognizer:))))
    }
    
    func getDirections(from: MKMapItem, to: MKMapItem, complete: @escaping ((MKPolyline) -> Void)) {
        let request = MKDirectionsRequest()
        request.source = from
        request.destination = to
        request.transportType = .walking
        
        let diretions = MKDirections(request: request)
        diretions.calculate { (response: MKDirectionsResponse?, error: Error?) in
            if let routes = response?.routes {
                self.map.insert(routes[0].polyline, at: 1, level: .aboveLabels)
                complete(routes[0].polyline)
            }
        }
    }
    
    func coordinatesFrom(polyline: MKPolyline) -> [CLLocationCoordinate2D] {
        var pontos: [CLLocationCoordinate2D] = []

        for _ in (0..<polyline.pointCount) {
            pontos.append(CLLocationCoordinate2D(latitude: 0, longitude: 0))
        }

        polyline.getCoordinates(&pontos, range: NSMakeRange(0, polyline.pointCount))

        return pontos
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.showWithAddress(address: textField.text!)
        return true
    }
    
    func showWithAddress(address: String) {
        self.searchMapItem(query: address) { (mapItem: MKMapItem?) in
            if mapItem != nil {
                self.map.showAnnotations([mapItem!.placemark], animated: true)
            }
        }
    }
    
    func searchMapItem(query: String, completionBlock: @escaping ((MKMapItem?) -> Void)) {
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = query
        request.region = map.region
        
        let search = MKLocalSearch(request: request)
        
        search.start { (response: MKLocalSearchResponse?, error: Error?) in
            if let map_items = response?.mapItems {
                if map_items.count > 0 {
                    completionBlock(map_items[0])
                }
                else {
                    completionBlock(nil)
                }
            }
            else {
                completionBlock(nil)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay is MKPolyline {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor(red: 0.2, green: 0.3, blue: 0.4, alpha: 1)
            renderer.lineWidth = 3
            return renderer
        }
        
        return MKTileOverlayRenderer(overlay: overlay)
    }

    func handleLongpress(recognizer: UILongPressGestureRecognizer) {
        if recognizer.state != .began {
            return
        }

        let touchPoint = recognizer.location(in: map)
        let coord = map.convert(touchPoint, toCoordinateFrom: map)
        
        let ponto = PontoAnnotation(ponto: Ponto(nome: "", fotos: [], audio: "", descricao: "", latitude: coord.latitude, longitude: coord.longitude, polyline: []), ordem: pontos.count)
        ponto.coordinate = coord
        
        map.removeAnnotations(map.annotations)
        
        pontos.append(ponto)
        map.showAnnotations(pontos, animated: true)
        
        if pontos.count > 1 {
            if let i = pontos.index(of: ponto) {
                let p = pontos[i-1]
                let from = MKMapItem(placemark: MKPlacemark(coordinate: p.coordinate))
                let to = MKMapItem(placemark: MKPlacemark(coordinate: coord))
                self.getDirections(from: from, to: to, complete: { (polyline: MKPolyline) in
                    ponto.info.polyline = self.coordinatesFrom(polyline: polyline)
                })
            }
        }
    }
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clear_all(_ sender: Any) {
        map.removeAnnotations(map.annotations)
        map.removeOverlays(map.overlays)
        
        pontos.removeAll()
    }
    
    @IBAction func clear_last(_ sender: Any) {
        if pontos.count > 1 {
            map.removeAnnotations([pontos.last!])
            map.remove(map.overlays.last!)
            
            pontos.removeLast()
        }
    }
    
    @IBAction func print(_ sender: Any) {
        print("[")
        for ponto in pontos {
            print("\t{\n\t\t\"nome\": \"\(ponto.order)\",")
            print("\t\t\"descricao\": \"\",")
            print("\t\t\"latitude\": \(ponto.coordinate.latitude),")
            print("\t\t\"longitude\": \(ponto.coordinate.longitude),")
            print("\t\t\"fotos\": [],")
            
            if let polyline = ponto.info.polyline {
                print("\t\t\"polyline\": [")
                for coordinate in polyline {
                    print("\t\t{\n\t\t\t\"latitude\": \(coordinate.latitude),")
                    print("\t\t\t\"longitude\": \(coordinate.longitude),")
                    print("\t\t},")
                }
                print("\t\t]")
            }
            print("\t},")
        }
        print("]")
    }
    
}
