//
//  MapController.swift
//  Ear Guide
//
//  Created by Paulo Cesar on 18/01/17.
//  Copyright © 2017 Paulo Cesar. All rights reserved.
//

import UIKit
import MapKit

class MapController: UIViewController, UITextFieldDelegate, MKMapViewDelegate, CLLocationManagerDelegate {
    
// MARK: -
    
    let dataImporter = DataImporter()
    
    var pontoController: PontoController?
    var selectedAnnotation: MKAnnotationView?
    
    @IBOutlet var map: MKMapView!
    @IBOutlet var shade: UIView!
    @IBOutlet var addressField: UITextField!
    @IBOutlet var pontoControllerBottomConstraint: NSLayoutConstraint!
    @IBOutlet var shadeBottonController: NSLayoutConstraint!
    @IBOutlet var containerHeight: NSLayoutConstraint!
    
    let locationManager = CLLocationManager()

// MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        map.delegate = self
        
//        map.isZoomEnabled = false
        
        if let pontos = dataImporter.pontos {
            
            let baseURL = Bundle.main.bundleURL.absoluteString
            let urlTemplate = baseURL.appending("tiles/{z}/{x}/{y}.png")
            let layer = MKTileOverlay(urlTemplate: urlTemplate)
            layer.canReplaceMapContent = true
            map.add(layer)
            
            self.map.showAnnotations(pontos, animated: false)
            
            //get routes online
//            for i in (0..<pontos.count-1) {
//                let from = MKMapItem(placemark: MKPlacemark(coordinate: pontos[i].coordinate))
//                let to = MKMapItem(placemark: MKPlacemark(coordinate: pontos[i+1].coordinate))
//                getDirections(from: from, to: to)
//            }
            
            //insert saved polyline on map
            for ponto in dataImporter.pontos! {
                if var polyline = ponto.info.polyline {
                    let mkpolyline = MKPolyline(coordinates: &polyline, count: polyline.count)
                    mkpolyline.title = "\(ponto.order)"
                    self.map.insert(mkpolyline, at: 1, level: .aboveLabels)
                }
            }
        }

        pontoControllerBottomConstraint.constant = -self.containerHeight.constant
        
        self.shade.isUserInteractionEnabled = true
        self.shade.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(MapController.hideShade)))
        
        locationManager.requestWhenInUseAuthorization()
        
        map.showsUserLocation = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        pontoController = segue.destination as? PontoController
    }
    
// MARK: -
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        map.showsUserLocation = true
    }
    
// MARK: -
    
    func getDirections(from: MKMapItem, to: MKMapItem) {
        let request = MKDirectionsRequest()
        request.source = from
        request.destination = to
        request.transportType = .walking
        
        let diretions = MKDirections(request: request)
        diretions.calculate { (response: MKDirectionsResponse?, error: Error?) in
            if let routes = response?.routes {
                self.map.insert(routes[0].polyline, at: 1, level: .aboveLabels)
            }
        }
    }
    
// MARK: -
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if let not_visited_points = dataImporter.pontos?.filter({ (p: PontoAnnotation) -> Bool in return !p.visited }) {
            for ponto in not_visited_points {
                if userLocation.coordinate.location().distance(from: ponto.coordinate.location()) < 50 {
                    ponto.visited = true
                    map.selectAnnotation(ponto, animated: true)
                    
                    for overlay in map.overlays {
                        if let title = overlay.title {
                            if let title_ = title {
                                if title_ == "\(ponto.order)" {
                                    if let r = map.renderer(for: overlay) as? MKPolylineRenderer {
                                        r.strokeColor = UIColor.with(r: 0, g: 127, b: 226)
                                    }
                                }
                            }
                        }
                    }
                    
                    return
                }
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let pa = annotation as? PontoAnnotation {
            
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationView") ?? MKAnnotationView()
            annotationView.image = UIImage(named: "ponto")
            annotationView.centerOffset = CGPoint(x: 0, y: -22)
            
            annotationView.tag = pa.order
            
            let label = UILabel(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 27, height: 27)))
            label.text = "\(pa.order+1)"
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 15)
            label.textColor = UIColor(red: 19.0/255, green: 150.0/255, blue: 254.0/255, alpha: 1)
            annotationView.addSubview(label)
            
            return annotationView
        }
        
        return nil
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay is MKPolyline {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor(white: 0.5, alpha: 0.7)
            renderer.lineWidth = 4
            return renderer
        }
        
        return MKTileOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        selectedAnnotation = view
        
        if let pontos = dataImporter.pontos {
            let annotation = pontos[view.tag]
            pontoController?.setupWith(ponto: annotation.info)
            
            if annotation.visited {
                view.image = UIImage(named: "ponto_selecionado")
                (view.subviews[0] as? UILabel)?.textColor = UIColor.white
            }
            
            self.shade.alpha = 0
            self.shade.isHidden = false
            
            self.containerHeight.constant = pontoController!.calculateHeight()
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.shade.alpha = 0.7
                self.pontoControllerBottomConstraint.constant = 0
                self.shadeBottonController.constant = self.containerHeight.constant
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    func hideShade() {
        map.deselectAnnotation(selectedAnnotation?.annotation, animated: true)
        selectedAnnotation = nil
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.shade.alpha = 0
            self.pontoControllerBottomConstraint.constant = -self.containerHeight.constant
            self.shadeBottonController.constant = 0
            self.view.layoutIfNeeded()
        }, completion: { (s: Bool) in
            self.shade.isHidden = true
        })
    }

}
