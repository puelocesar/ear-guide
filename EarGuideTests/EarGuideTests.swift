//
//  EarGuideTests.swift
//  EarGuideTests
//
//  Created by Paulo Cesar on 19/01/17.
//  Copyright © 2017 Paulo Cesar. All rights reserved.
//

import XCTest

class EarGuideTests: XCTestCase {
    
    func testImportacao() {
        let data = DataImporter()
        XCTAssert(data.pontos != nil)
        XCTAssert(data.pontos!.count > 0)
    }
    
    func testPerformance() {
        self.measure {
            let data = DataImporter()
            
            for ponto in data.pontos! {
                print(ponto)
            }
        }
    }
    
}
